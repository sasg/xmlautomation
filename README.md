#Drupal Module Inventory XML Automation
---------------------------------------
This script gathers module inventory information from the servers (defined by our master config file) and generates a valid XML document from that information. This document can be used with Feeds Import to make a Drupal view of our entire module inventory.

Viewable here: drupal.em.arizona.edu/module-inventory

##Requirements:
1. Python 2.7
2. python-virtualenv, python-dev, python-pip (installed using apt-get)
3. fabric (installed via pip)
4. Access to master configuration file on 007 (/usr/sbin/master-pyconfig)

##How to run:
	python modulesxml.py
This will generate a file called modules_xml_output which can then be used with the importer