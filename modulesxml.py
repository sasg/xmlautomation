# Drupal Module Inventory XML Generator 
# Adam Soll
# 11-30-2015
"""
This script will gather module inventory information from Drupal servers and 
generate a valid XML document for use with Drupal Feeds Import.
"""


import fabric
import sys
import os
from xml.dom.minidom import parseString
from fabric.api import *
sys.path.append("/usr/sbin/master-pyconfig/") # Allow import from master-pyconfig
from config import hosts, user, password

# Switching encoding over to utf8
reload(sys)
sys.setdefaultencoding("utf8")


# Setting environment variables for fabric
env.user = user
env.password = password
env.hosts = hosts


def get_server():
    """Get server name to be used in directory naming"""
    # Runs "hostname" command on each host
    return str(run("hostname"))


def get_sites():
    """Get sites located in /var/www/web/sites/"""
    # Runs "cd /var/www/web/sites && ls" on each host and
    # Stores it in the ls variable
    ls = str(run("cd /var/www/web/sites && ls"))

    # Cleaning up ls data
    for item in ls:
        item.replace("\t", " ")

    ls = ls.split()
    # Creating empty site_list to store site names
    site_list = []

    for item in ls:
        # If "edu" is in the sitename it is assumed to be a site
        # (as opposed to a non-site folder in /var/www/web/sites/)
        if "edu" in item and "tgz" not in item and "tar" not in item:
            site_list.append(item)

    return site_list


def get_pmlist_text():
    """Get drush pm-list output from each site in site_list"""
    # Grabbing host name
    hostname = get_server()
    # Grabbing list of sites
    site_list = get_sites()

    # For each site contained in site_list
    for site in site_list:
        # Runs pm-list command with proper flags in the correct directory
        sudo("cd /var/www/web/sites/"+str(site)+" && drush pm-list --no-core \
            --status=enabled --type=Module --format=csv > "+str(site)+"_output.txt")
        # Gets the text output of the pm-list and puts it in the "local_path" set directory
        get(remote_path = "/var/www/web/sites/"+str(site)+"/"+str(site)+"_output.txt",
            local_path = "./"+hostname+"/"+str(site), use_sudo=True)
        # Removes extraneous text files left over after process
        run("rm -f /var/www/web/sites/"+str(site)+"/"+str(site)+"_output.txt")   
    
    fabric.network.disconnect_all()

    return None


class ModuleInventory:
    """ModuleInventory contains the methods for processing pm-list output"""

    def __init__(self):
        """Initializes instance variables for ModuleInventory"""
        # Self.modules_dict stores modules/sites in key:value(s) pairs
        self.modules_dict = {}
        # Self.drupal_dirs is the list of folders in the current directory
        # To loop through (after grabbing text from servers)
        self.drupal_dirs = []


    def get_dirs(self):
        """Gets directories housing pm-list output"""
        # Stores the contents of the current directory in a list
        directory = os.listdir("./")

        # If a folder has "drupal" in the filename, add it to self.drupal_dirs list
        for item in directory:
            if "drupal" in item and "d7" in item:
                self.drupal_dirs.append(item)

        return None


    def process_by_module(self):
        """Builds dictionaries in terms of site by module"""
        # Opens each file within each directory listed in self.drupal_dirs
        for _dir in self.drupal_dirs:
            for _file in os.listdir(_dir):
                with open(_dir+"/"+_file) as f:
                    # Splits file content by line
                    data = f.read().split("\n")
                    for item in data:
                        # Splits content further by comma
                        mod = item.split(",")
                        # Must be 3 parts after split (package, module name, version num)
                        # in order to be counted as a module
                        if len(mod) == 3:
                            name_ver = mod[1] + ", " + mod[2]
                            if name_ver not in self.modules_dict.keys():
                                # If site is not already in self.modules_dict
                                self.modules_dict[name_ver] = []
                            self.modules_dict[name_ver].append(_file + " (" + _dir.split("-")[1].upper()+")")

        return None


    def write_xml_modules(self):
        """Writes the data from self.modules_dict to xml"""
        xml_modules_string = "<modules>\n"
        
        for key in sorted(self.modules_dict):
            name = key.split(",")[0]
            version = key.split(",")[1][1:]
            xml_modules_string += "\t<module>\n"
            xml_modules_string += "\t\t<name>" + str(name) + "</name>\n"
            xml_modules_string += "\t\t<version>" + str(version) + "</version>\n"
            xml_modules_string += "\t\t<sites>\n"
            xml_modules_string += "\t\t\t<![CDATA[\n\t\t\t\t<ul>\n"
            for item in self.modules_dict[key]:
                xml_modules_string += "\t\t\t\t<li>" + str(item) + "</li>\n"
            xml_modules_string += "\t\t\t\t</ul>\n"
            xml_modules_string += "\t\t\t]]>\n\t\t</sites>\n\t</module>\n"
        xml_modules_string += "</modules>"
        
        xml_output = open("modules_xml_output.xml", "w")
        xml_output.write(xml_modules_string)
        xml_output.close()

        return None


# This is what is executed when the script is run
if __name__=="__main__":
    # Grabs module info from servers
    execute(get_pmlist_text)

    inv = ModuleInventory()
    inv.get_dirs()
    inv.process_by_module()
    inv.write_xml_modules()
